using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Xml.Serialization;
using System.Diagnostics;


namespace WPSyntaxFramework.LiveWriter.Plugins
{
    public partial class FrmLiveWriterPlug : Form
    {

        Profile profile = ProfileManager.Instance;
        Profile _old_profile = new Profile();
        GlobalSet _global_set;
        private string _content;
        //bool _is_profile_changed = false;

        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }
        public FrmLiveWriterPlug()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(FrmLiveWriterPlug_FormClosing);
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory + "\\Plugins\\WP-Syntax.LiveWriter.Plugins\\");
            string global_set_file_name = "main.config";
            //获取所有方案和语言
            using (FileStream fs = new FileStream(global_set_file_name, FileMode.Open))
            {
                XmlSerializer ser = new XmlSerializer(typeof(GlobalSet), new Type[] { typeof(GlobalSet) });
                _global_set = ser.Deserialize(fs) as GlobalSet;
            }
            

            //下拉列表初始化
            foreach (string lang in _global_set.Langs)
            {
                cboLanguage.Items.Add(lang);
            }
            cboLanguage.Text = profile.Language;

            foreach (Target tar in _global_set.Targets)
            {
                cboWebSite.Items.Add(tar.Name);
            }
            cboWebSite.Text = profile.Target;

            checkBoxExpand.Checked = profile.IsExpand;
            checkBoxShowLineNumber.Checked = profile.IsShowLineNumber;

            //保存之前的配置
            _old_profile = (Profile)profile.Clone();
        }

        void FrmLiveWriterPlug_FormClosing(object sender, FormClosingEventArgs e)
        {
            profile.Language = cboLanguage.Text;
            profile.Target = cboWebSite.Text;
            profile.IsExpand = checkBoxExpand.Checked;
            profile.IsShowLineNumber = checkBoxShowLineNumber.Checked;
            if ((profile.Language !=_old_profile.Language)
                || (profile.Target != _old_profile.Target)
                || (profile.IsExpand!= _old_profile.IsExpand)
                || (profile.IsShowLineNumber != _old_profile.IsShowLineNumber)
                )
            {
                ProfileManager.Save();
            }
        }


        public FrmLiveWriterPlug(ref string content)
            : this()
        {
            this._content = content;
        }


        private void LiveWriterPlug_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + Assembly.GetExecutingAssembly().GetName().Version.ToString();


        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (cboWebSite.Text == "")
            {
                MessageBox.Show("请选择方案");
                return;
            }
            if (cboLanguage.Text == "")
            {
                MessageBox.Show("请选择语言");
                return;
            }
            string cmd = "";
            foreach (Target tar in _global_set.Targets)
            {
                if (tar.Name == cboWebSite.Text)
                {
                    cmd = tar.Command;
                    break;
                }
            }


            string rest = GetHtml(txtContent.Text,checkBoxShowLineNumber.Checked,
                checkBoxExpand.Checked, cboLanguage.Text, cmd);
            if(rest == null)
            {

            }
            else
            {
                this._content = rest;
                //txtContent.Text = rest;//test

                this.Close();
            }
        }
        public static void Main()
        {
            Application.Run(new FrmLiveWriterPlug());
        }

        public string GetHtml(string content, bool is_show_line_number, bool is_allow_expand, string language, string cmd)
        {
            string new_content = content;
            if (cmd == "")
            {
                new_content = String.Format("<pre escaped=\"true\" lang=\"{0}\" line=\"{1}\" >\n{2}</pre><p></p>", language, Convert.ToInt32(is_show_line_number), content);
            }
            else
            {
                //string tmp;
                string path = System.IO.Path.GetTempPath() + "\\WP-Syntax.LiveWriter.Plugins.ini";
                //FileStream outfile = File.OpenWrite(path);
                File.WriteAllText(path, 
                    String.Format("language={0}\nline={1}\nexpand={2}\n[content]\n{3}",
                    language, Convert.ToInt32(is_show_line_number),Convert.ToInt32(is_allow_expand), content)
                    ,Encoding.UTF8
                );
                //outfile.Close();

                Process p = new Process();
                //设定程序名
                p.StartInfo.FileName = "cmd.exe";
                //确定程式命令行
                p.StartInfo.Arguments = "/c " + cmd+" "+path;    
                //关闭Shell的使用
                p.StartInfo.UseShellExecute = false;
                //重定向标准输入
                //p.StartInfo.RedirectStandardInput = true;
                //重定向标准输出
                p.StartInfo.RedirectStandardOutput = true;
                //重定向错误输出
                p.StartInfo.RedirectStandardError = true;
                //设置不显示窗口
                p.StartInfo.CreateNoWindow = true;
                try
                {
                    p.Start();
                    p.WaitForExit();
                    if (p.ExitCode != 0)
                    {
                        string result = p.StandardError.ReadToEnd();
                        throw new Exception(result);
                    }
                    else
                    {
                        new_content = File.ReadAllText(path, Encoding.UTF8);
                    }
                }
                catch (System.Exception e)
                {
                    MessageBox.Show(e.Message, "Error");
                    new_content = null;
                }
#if !DEBUG
                File.Delete(path);
#endif
            }
            return new_content;
        }



        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://shashanzhao.com/wp-syntax-livewriter");

        }



    }
}