#WP-Syntax.LiveWriter
##--Windows Live Writer的语法高亮插件
可执行文件下载地址: [http://shashanzhao.com/wp-syntax-livewriter](http://shashanzhao.com/wp-syntax-livewriter)

#介绍:

由于在wordpress中编辑很慢,所以就想找一款客户端软件,最后发现Windows Liver Writer不错. 后来发现这个writer还支持插件, 就想找一个支持语法高亮的插件, 且是依赖于wordpress的wp-syntax的插件,结果没有找到. 索性就自己做了这个插件.

#系统要求
Windows

需要安装以下组件:

1. Windows Live Writer
2. .Net Framework 2.0

如果使用common解决方案,则还需要安装以下组件:

1. ruby 2.0或以上版本
2. ruby库rouge

#安装方法
1. 将生成的文件WP-Syntax.LiveWriter.Plugins.dll放在Windows Live的安装目录下的Writer\Plugins\, 
2. 在Writer\Plugins\目录下新建WP-Syntax.LiveWriter.Plugins目录,将工程目录下的other目录下所有文件复制到Writer\Plugins\WP-Syntax.LiveWriter.Plugins目录

#使用方法
插入代码到安装WP-syntax插件的wordpress中

1. 在Windows Live Writer的插入菜单下选择插入代码块,弹出对话框
2. 选择解决方案:WP-syntax
3. 把代码粘贴对话框里,点击插入按钮

插入代码到通用的博客中

1. 在Windows Live Writer的插入菜单下选择插入代码块,弹出对话框
2. 选择解决方案:common,  这个方案类似于github上的显示效果
3. 把代码粘贴对话框里,点击插入按钮

注意:通用方案需要将Windows Live\Writer\Plugins\WP-Syntax.LiveWriter.Plugins\common.css放到服务器上,让博客引用这个common.css文件


#代码介绍
在C/C++,Ruby虽然很强悍,但是C#毕竟新东西,之前看过一遍C#宝典,主要是看思想,没记住多少设计规则,用法等, 所以这个插件就是依葫芦画瓢写的.

此插件参考:http://www.cnblogs.com/yaoshiyou/archive/2009/11/25/1610901.html, 就是把工程拿来修改修改, 既然是修改修改,就需要简单,简单才不会犯错. 所以把参考工程中的语法高亮库给去掉了,把设置对话框给去掉了.

以下是一些文件介绍:

FrmLiveWriterPlug.cs - 主界面

GlobalSet.cs - 软件配置类

LiveWriterAdapter.cs - 负责与Writer插件连接

Profile.cs - 用户配置类

#原理
原理就是Writer调用插件,插件弹出对话框,让用户输入代码,然后对代码进行处理,将处理完的代码返回给Writer.

关于处理用户输入的代码,在使用wp-syntax插件时是非常简单的,只要将代码前加`<pre lang="ruby" line="1" escaped="true">`后面加`</pre>`即可,其中lang的值表示语言, line表示是否显示行号.这个在插件内部已经实现,只要选择方案WP-Syntax即可,对于不用wp-syntax插件的人来说我也为其准备了一套方案,那就是通过ruby的rouge库所生产的GitHub主题的语法高亮方案.

#如何扩展以使用我的网站的语法高亮格式
自定义扩展可以通过修改配置文件来添加解决方案,配置文件是main.config.

其中在元素Langs下有很多string元素,添加一个新的string元素就是添加一个新的语言.
比如`<string>sql</string>`就是添加sql语言

在Targets元素下是解决方案,

目前有两个,可以自己添加

    <Target>
      <Name>WP-Syntax</Name>
      <Command></Command>
    </Target>
    <Target>
      <Name>common</Name>
      <Command>ruby make_syntax.rb</Command>
    </Target>
   
比如新添加一种解决方案是针对某个网站的.

    <Target>
      <Name>XXX</Name>
      <Command>python xxx.py</Command>
    </Target>

其中的command填写生成高亮语法的程序,可以是脚本或者exe,能运行就行.

插件会将一个包含选项和用户输入的代码的文件的路径当成参数传给command,(参数比如是:r:\WP-Syntax.LiveWriter.Plugins.ini,整个命令行可能是:python xxx.py r:\WP-Syntax.LiveWriter.Plugins.ini)

用户程序需要解析这个文件,获取选项和代码,然后把语法高亮的代码再写入这个文件并且返回0,

插件判断是否返回0,如果是就读取那个文件,并将文件内容插入writer.

注:文件WP-Syntax.LiveWriter.Plugins.ini相关信息介绍

    language=ruby
    line=1
    expand=1
    [content]
    int i;

第一个表示用户选择的语言,第二个表示用户选择是否显示行号(1表示显示行号,0表示不显示行号),第三个表示用户选择是否可以展开代码(1表示有展开功能,0表示没有展开功能),content下面的就是用户输入的代码.