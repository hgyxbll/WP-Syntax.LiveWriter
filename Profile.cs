using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace WPSyntaxFramework.LiveWriter.Plugins
{
    #region ProfileManager
    class ProfileManager
    {

        private static Profile _instance;

        internal static Profile Instance
        {
            get
            {
                if (_instance == null)
                {
                    if (File.Exists(profileName))
                    {
                        Load();
                    }
                    else
                    {
                        _instance = new Profile();
                    }
                }
                return _instance;
            }
            set { _instance = value; }
        }

        private static string profileName =
            System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
            + "\\WP-Syntax.LiveWriter.Plugins.config";

        public static void Save()
        {
            using (FileStream fs = new FileStream(profileName, FileMode.Create))
            {
                XmlSerializer ser = new XmlSerializer(_instance.GetType());
                ser.Serialize(fs, _instance);
            }
        }

        public static void Load()
        {
            using (FileStream fs = new FileStream(profileName, FileMode.Open))
            {
                XmlSerializer ser = new XmlSerializer(typeof(Profile));
                _instance = ser.Deserialize(fs) as Profile;
            }
        }
    }
    #endregion
    [Serializable]
    public class Profile
    {
        //选择的语言
        private string _language;
        public string Language
        {
            get { return _language; }
            set { _language = value; }
        }

        //选择的解决方案
        private string _target;
        public string Target
        {
            get
            {
                return _target;
            }
            set { _target = value; }
        }

        private bool _isExpand;
        /// <summary>
        /// 是否折叠代码
        /// </summary>
        public bool IsExpand
        {
            get { return _isExpand; }
            set { _isExpand = value; }
        }

        private bool _is_show_line_number;
        /// <summary>
        /// 是否显示行号
        /// </summary>
        public bool IsShowLineNumber
        {
            get { return _is_show_line_number; }
            set { _is_show_line_number = value; }
        }

        public Profile()
        {
 
        }
        public object Clone()
        {

            BinaryFormatter bf = new BinaryFormatter();

            MemoryStream ms = new MemoryStream();

            bf.Serialize(ms, this);

            ms.Seek(0, SeekOrigin.Begin);

            return bf.Deserialize(ms);

        }
    }
}
