require 'rouge' #语法高亮库

in_file = File.open ARGV[0],"r:utf-8"
is_find_content = false

language = nil
is_show_line_number = false
is_allow_expand = false
content = ""

in_file.each_line do |line|
	if is_find_content
		content += line		
	elsif line == "[content]\n"
		is_find_content = true
	else
		if line=~/language=(.+)/
			language = $1
		elsif line=~/line=(.+)/
			if $1 == "1"
				is_show_line_number = true
			end
		elsif line=~/expand=(.+)/
			if $1 == "1"
				is_allow_expand = true
			end
		end
	end
end

#content = "<pre> #{content} </pre>"
#使用rouge库生成高亮的html代码
formatter = Rouge::Formatters::HTML.new(:css_class => 'highlight')
#formatter = Rouge::Formatters::HTML.new()
lexer_class = Rouge::Lexer.find language
if lexer_class
	lexer = lexer_class.new
else
	lexer = Rouge::Lexers::PlainText
end
#puts content.encoding
content = formatter.format(lexer.lex(content))
# Get some CSS
#css = Rouge::Themes::Github.render(:scope => '.highlight')
#css = "<style type=\"text/css\">#{css}</style>"
content = "#{content}<p></p>"

File.write ARGV[0],content,:encoding=>"utf-8"
exit 0

#test
puts language
puts is_show_line_number
puts is_allow_expand
puts content
File.write "text.html",content,:encoding=>"utf-8"