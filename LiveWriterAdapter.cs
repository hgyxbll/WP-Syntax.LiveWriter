using System;
using System.Collections.Generic;
using System.Text;
using WindowsLive.Writer.Api;
using System.Windows.Forms;

namespace WPSyntaxFramework.LiveWriter.Plugins
{
    [WriterPlugin("75713A8B-C177-4148-894D-F1FD3C524906", "��������",
    PublisherUrl = "http://shashanzhao.com/wp-syntax-livewriter",
    //ImagePath = "image.main.bmp",
    Description = "��������")]
    [InsertableContentSource("��������")]

    public class LiveWriterAdapter : ContentSource
    {
        public override DialogResult CreateContent(IWin32Window dialogOwner, ref string content)
        {
            using (FrmLiveWriterPlug fr = new FrmLiveWriterPlug())
            {
                fr.ShowDialog();
                content = fr.Content;
                return (!string.IsNullOrEmpty(fr.Content) ? DialogResult.OK : DialogResult.No);
            }

        }
        public override void EditOptions(IWin32Window dialogOwner)
        {

        }
    }
}
