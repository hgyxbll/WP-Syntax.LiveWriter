using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;


namespace WPSyntaxFramework.LiveWriter.Plugins
{
    public class Target
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _command;
        public string Command
        {
            get { return _command; }
            set { _command = value; }
        }
        public Target()
        {

        }
        public Target(string name, string command)
        {
            _name = name;
            _command = command;
        }
    }
    public class GlobalSet
    {
        //所有支持的语言
        private List<string> _langs = new List<string>();
        public List<string> Langs
        {
            get { return _langs; }
            set { _langs = value; }
        }

        //所有支持的方案
        private List<Target> _targets = new List<Target>();
        public List<Target> Targets
        {
            get { return _targets; }
            set { _targets = value; }
        }
    }
}
